#include <Servo.h>
#include <LiquidCrystal.h>


LiquidCrystal lcd(11, 10, 9, 8, 7, 6);
Servo motor_left;
Servo motor_right;
float pos_left = 0.0;
float pos_right = 0.0;
float step = 1.0;
int ammunition = 10;

void updateLCD() {
  lcd.clear();
  lcd.print("Ammo: " + String(ammunition));
}

void setup() {
  motor_left.attach(2);
  motor_right.attach(3);
  motor_left.write(pos_left);
  motor_right.write(pos_right);

  pinMode(A5, INPUT_PULLUP);  // Right
  pinMode(A4, INPUT_PULLUP);  // Up
  pinMode(A3, INPUT_PULLUP);  // Down
  pinMode(A2, INPUT_PULLUP);  // Left
  pinMode(A1, INPUT_PULLUP);  // Fire

  pinMode(12, OUTPUT);         // Fire LED
  pinMode(4, OUTPUT);         // Buzzer

  Serial.begin(115200);
  lcd.begin(16, 2);
  updateLCD();
}

void fire() {
  digitalWrite(12, HIGH);
  digitalWrite(4, HIGH);
  delay(1000);
  digitalWrite(12, LOW);
  digitalWrite(4, LOW);
  ammunition--;
  updateLCD();
}

void loop() {
  if (!digitalRead(A2))
  {
    if (pos_right>0)
    {
      motor_right.write(pos_right);
      pos_right-=step; // Decrement "pos" of "step" value
      delay(5); // Wait 5ms for the arm to reach the position
    }
  }
  if (!digitalRead(A5))
  {
    if (pos_right<180)
    {
      motor_right.write(pos_right);
      pos_right+=step; // Decrement "pos" of "step" value
      delay(5); // Wait 5ms for the arm to reach the position
    }
  }
  if (!digitalRead(A3))
  {
    if (pos_left<180)
    {
      motor_left.write(pos_left);
      pos_left+=step; // Decrement "pos" of "step" value
      delay(5); // Wait 5ms for the arm to reach the position
    }
  }
  if (!digitalRead(A4))
  {
    if (pos_left>0)
    {
      motor_left.write(pos_left);
      pos_left-=step; // Decrement "pos" of "step" value
      delay(5); // Wait 5ms for the arm to reach the position
    }
  }

  if (!digitalRead(A1) && ammunition > 0)
  {
    fire();
  }
}
